wx.setNavigationBarTitle({
  title: '汪汪的查询系统'
})

Page({
  data: {
    valueEmpty: true,
    userName: '',
    hotSearch: [
      { name: "詹姆斯" },
      { name: "科比" },
      { name: "库里" },
      { name: "莱昂纳德" },
      { name: "杜兰特" },
      { name: "韦斯特布鲁克" },
      { name: "哈登" },
      { name: "保罗" },
      { name: "欧文" },
      { name: "托马斯" }
    ],
    showList: true,
    showLoading: false,
    noDataTip: false
  },
  closeTunnel() {
    if (this.tunnel) {
      this.tunnel.close();
    }
    this.setData({ tunnelStatus: 'closed' });
  },
  cancelSystem() {
    this.closeTunnel()
    wx.navigateTo({ url: '../index/index' })
  },
  sendRequest(e) {
    var value = e.detail.value
    if(value!=='') {
      this.setData({
        valueEmpty: false,
        userName: value,
        showList: false,
        showLoading: true,
        noDataTip: false
      })
    }
    var timeout = setTimeout((function callback(){
      this.setData({
        showList: false,
        showLoading: false,
        noDataTip: true
      })
    }).bind(this),2000)
    console.log(value)
    // wx.request({
    //   url: 'https://dingjia.guchele.com/api/lib/Coupon_activity/detail',
    //   data: {
    //     activity_id: '1'
    //   },
    //   header: {
    //     "Content-Type": "application/json"
    //   },
    //   success: function (res) {
    //     console.log(res.data)
    //   }
    // })
  },
  deleteValue() {
    this.setData({
      userName: "",
      valueEmpty: true,
      showList: true,
      showLoading: false,
      noDataTip: false
    })
  }
})
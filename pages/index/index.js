
wx.setNavigationBarTitle({
  title: ' '
})

Page({

  closeTunnel() {
    if (this.tunnel) {
      this.tunnel.close();
    }

    this.setData({ tunnelStatus: 'closed' });
  },

  testQuery() {
    this.closeTunnel();
    wx.navigateTo({ url: '../system/system' });
    //  wx.showToast({
    //    title: '测试',
    //  })
  }
})